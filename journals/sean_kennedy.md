## October 4th

Today gave me a chance to explore what it is like to create basic CRUD endpoints using FastAPI and PostgreSQL.

## October 5th

Today we discovered some limitations that may arise due to using an SQL database, and it gave us a chance to start exploring MongoDB

## October 6th

I started combing the example bookstore application built by Curtis in order to apply similar principles to our project. This gave me a chance to understand how to set up the docker-compose.yaml file for MongoDB. It also gave me the chance to get familiar with databases and collections, how they are generated, and how they interact with the volume used to store the database.

## October 10th

As a group, we did pair programming, where Devin was in the driver seat. Mike, Thomas, and I offered support as we started designing the backend routes for our API.

## October 11th

As a group, we did pair programming and started implementing the third party api to start storing cards for specific users in the database. Devin and I sat in the driver’s seat for different pieces of the pair programming with Mike and Thomas offering support.

## October 12th

As a group, we spent time finishing some of our backend routes for our API and debugging issues that prevented routes from accessing the database correctly.

## October 13th

Today, I spent time trying to understand more about redux and redux toolkit. I set up a basic Query using redux toolkit that will allow users to get a list of cards from our third party API, but I could not figure out how to create a slice for the search term that needs to be passed into the query in order to generate these results dynamically.

## October 14th

Today, I spent time building out the basic search feature for the Scryfall API. In order to do this, I had to set up a redux store and create a slice to manage the state of the search term being entered in the navbar. This is because the data entered into the navbar has to be shared with its sibling component SearchResults.js

## October 17th

Today I spent time building out the advanced search feature for the Scryfall API. I had to figure out how to take the data from the form that gets submitted in the advanced search section and build a string that could successfully query the scryfall API.

## October 18th

Today I spent time setting up global state management for the sign in and sign up form. This gave me a chance to explore what it is like to create slices and actions as well as dispatching those actions to various components in an application.

## October 19th

Today we spent time trying to debug issues that were being generated when a user logged in or signed up. We had to look into the routes we set up for RTK Query and we learned that one of our routes was being overwritten due to using the same variable name that was being used for a previous route.

## October 20th

Today I spent time refining the search feature in order to allow users to add cards to their decks/collection from the search page. I also set up a page to view users decks and the details for a particular deck.

## October 24th

Today, I spent a lot of time working with Devin on updating some of the methods that enable the front end to query the database. We worked on setting up RTK Query routes to query the database and replaced fetches where appropriate.

## October 25th

Today we spent time merging several of our features together onto the development branch. Some time was spent debugging bugs that resulted from merge conflicts.

## October 26th

Today we spent time working as a group on writing unit tests. A lot of time was spent learning how to set up unit tests for features that require authentication. This was a great learning process for us as we got a chance to learn how to spoof a fake user for unit tests.

## October 27th

Today we spent a lot of time as a group working on deployment. We got more familiar with setting up our CI/CD pipelines and resolving linting errors that were causing our deployment tests to fail

## October 28th

We did it! Thanks Daniel Billotte!
